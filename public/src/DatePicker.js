import React, { Fragment, useState } from "react";
import ReactDom from 'react-dom';
import 'react-calendar/dist/Calendar.css';
import { KeyboardDatePicker } from "@material-ui/pickers";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { DatePicker } from "@material-ui/pickers";
import App from "/public/src/App";

const StaticDatePicker = () => {
  const [date, changeDate] = useState(new Date());

  return (
    <>
    <div>
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <DatePicker
        autoOk
        variant="static"
        openTo="date"
        value={date}
        onChange={changeDate}
      />
      <KeyboardDatePicker
          placeholder="2000/01/01"
          value={date}
          onChange={date => changeDate(date)}
          format="yyyy/MM/dd"
        />
      </MuiPickersUtilsProvider>
      </div>
      <App/>
    </>
  );
};