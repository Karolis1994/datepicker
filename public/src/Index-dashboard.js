import React, { Fragment, useState } from "react";
import ReactDom from 'react-dom';
import 'react-calendar/dist/Calendar.css';
import App from "/public/src/App";

//Static datepicker
const StaticDatePicker = () => {
  const [date, changeDate] = useState(new Date());

  return (
    <>
      <App/>
    </>
  );
};

ReactDom.render(
  <StaticDatePicker/>,
  document.getElementsByTagName('root')[0]
);

