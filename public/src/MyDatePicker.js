import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import '/assets/CustomDatePicker.css';

let oneDay = 60 * 60 * 24 * 1000;
let todayTimestamp = Date.now() - (Date.now() % oneDay) +
  (new Date().getTimezoneOffset() * 1000 * 60);
let inputRef = React.createRef();
export default class MyDatePicker extends Component {

  state = {
    getMonthDetails: []
  }

  constructor() {
    super();
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth();
    this.state = {
      year,
      month,
      selectedDay: todayTimestamp,
      monthDetails: this.getMonthDetails(year, month)
    }
  }

  componentDidMount() {
    window.addEventListener('click', this.addBackDrop);
    this.setDateToInput(this.state.selectedDay);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.addBackDrop);
  }

  addBackDrop = e => {
    if (this.state.showDatePicker && !ReactDOM.findDOMNode(this).contains(e.target)) {
      this.showDatePicker(false);
    }
  }

  showDatePicker = (showDatePicker = true) => {
    this.setState({ showDatePicker })
  }

  /**
   *  Core
   */

  daysMap = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  monthMap = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];

  getDayDetails = args => {
    let date = args.index - args.firstDay;
    let day = args.index % 7;
    let prevMonth = args.month - 1;
    let prevYear = args.year;
    if (prevMonth < 0) {
      prevMonth = 11;
      prevYear--;
    }
    let prevMonthNumberOfDays = this.getNumberOfDays(prevYear, prevMonth);
    let _date = (date < 0 ? prevMonthNumberOfDays + date : date % args.numberOfDays) + 1;
    let month = date < 0 ? -1 : date >= args.numberOfDays ? 1 : 0;
    let timestamp = new Date(args.year, args.month, _date).getTime();
    return {
      date: _date,
      day,
      month,
      timestamp,
      dayString: this.daysMap[day]
    }
  }

  getNumberOfDays = (year, month) => {
    return 40 - new Date(year, month, 40).getDate();
  }

  getMonthDetails = (year, month) => {
    let firstDay = (new Date(year, month)).getDay();
    let numberOfDays = this.getNumberOfDays(year, month);
    let monthArray = [];
    let rows = 6;
    let currentDay = null;
    let index = 0;
    let cols = 7;

    for (let row = 0; row < rows; row++) {
      for (let col = 0; col < cols; col++) {
        currentDay = this.getDayDetails({
          index,
          numberOfDays,
          firstDay,
          year,
          month
        });
        monthArray.push(currentDay);
        index++;
      }
    }
    return monthArray;
  }

  isCurrentDay = day => {
    return day.timestamp === todayTimestamp;
  }

  isSelectedDay = day => {
    return day.timestamp === this.state.selectedDay;
  }

  getDateFromDateString = dateValue => {
    let dateData = dateValue.split('-').map(d => parseInt(d, 10));
    if (dateData.length < 3)
      return null;

    let year = dateData[0];
    let month = dateData[1];
    let date = dateData[2];
    return { year, month, date };
  }

  getMonthStr = month => this.monthMap[Math.max(Math.min(11, month), 0)] || 'Month';

  getDateStringFromTimestamp = timestamp => {
    let dateObject = new Date(timestamp);
    let month = dateObject.getMonth() + 1;
    let date = dateObject.getDate();
    return dateObject.getFullYear() + '-' + (month < 10 ? '0' + month : month) + '-' +
      (date < 10 ? '0' + date : date);
  }

  setDate = dateData => {
    let selectedDay = new Date(dateData.year, dateData.month - 1, dateData.date).getTime();
    this.setState({ selectedDay })
    if (this.props.onChange) {
      this.props.onChange(selectedDay);
    }
  }

  updateDateFromInput = () => {
    let dateValue = inputRef.current.value;
    let dateData = this.getDateFromDateString(dateValue);
    if (dateData !== null) {
      this.setDate(dateData);
      this.setState({
        year: dateData.year,
        month: dateData.month - 1,
        monthDetails: this.getMonthDetails(dateData.year, dateData.month - 1)
      })
    }
  }

  setDateToInput = (timestamp) => {
    let dateString = this.getDateStringFromTimestamp(timestamp);
    inputRef.current.value = dateString;
  }

  onDateClick = day => {
    this.setState({ selectedDay: day.timestamp }, () => this.setDateToInput(day.timestamp));
    if (this.props.onChange) {
      this.props.onChange(day.timestamp);
    }
  }

  setYear = offset => {
    let year = this.state.year + offset;
    let month = this.state.month;
    this.setState({
      year,
      monthDetails: this.getMonthDetails(year, month)
    })
  }

  setMonth = offset => {
    let year = this.state.year;
    let month = this.state.month + offset;
    if (month === -1) {
      month = 11;
      year--;
    } else if (month === 12) {
      month = 0;
      year++;
    }
    this.setState({
      year,
      month,
      monthDetails: this.getMonthDetails(year, month)
    })
  }

  /**
   *  Renderers
   */

  renderCalendar() {
    let days = this.state.monthDetails.map((day, index) => {
      return (
        <Button
        
          style={{ width: 100 / 7 + '%' }}
          disabled={day.month !== 0}
          active
          variant={
            (this.isCurrentDay(day) && (day.month === 0) ? '' : 'outline-') +
            (this.isSelectedDay(day) && (day.month === 0) ? 'success' : 'info')}
          onClick={() => this.onDateClick(day)}
          key={index}>

          <Col>
            <span>
              {day.date}
            </span>
          </Col>
        </Button>

      )
    })

    return (
      <Table>
        <Row>
          {['SUNDAY', 'MONDAY', 'TUESDAY', 'WENSDAY', 'THUSDAY', 'FRIDAY', 'SATURDAY'].map((d, i) =>
            <Col key={i} >{d.substr(0, 1)}</Col>)}
        </Row>
        <tr>
          <td>
            {days}
          </td>
        </tr>
      </Table>
    )
  }
  render() {
    return (
      <div>
        <div onClick={() => this.showDatePicker(true)}>
          <input onChange={this.updateDateFromInput} ref={inputRef} />
        </div>
        {this.state.showDatePicker ? (
          <Table className='DatePicker'>
            <thead className="text-center">
              <Button variant="outline-info" onClick={() => this.setYear(-1)}>
                {'<<'}
              </Button>
              <Button variant="outline-info" onClick={() => this.setMonth(-1)}>
                {'<'}
              </Button>
              <Button variant="secondary" className='DatePicker__MonthButton'>
                {this.state.year} {this.getMonthStr(this.state.month)}
              </Button>
              <Button variant="outline-info" onClick={() => this.setMonth(1)}>
                {'>'}
              </Button>
              <Button variant="outline-info" onClick={() => this.setYear(1)}>
                {'>>'}
              </Button>
            </thead>
            <tbody>
              {this.renderCalendar()}
            </tbody>
          </Table>
        ) : ''}
      </div>
    )
  }
}